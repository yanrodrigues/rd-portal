export const environment = {
  production: true,
  BASE_URL: 'https://rd-portal-api.herokuapp.com/',
  token_session: 'TOKEN'
};
