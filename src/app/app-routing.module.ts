import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
    {
        path: 'login',
        loadChildren: './authentication/authentication.module#AuthenticationModule'
    },
    {
        path: 'dashboard',
        loadChildren: './dashboard/dashboard.module#DashboardModule'
    },
    {
        path: 'post',
        loadChildren: './post/post-view.module#PostViewModule'
    },
    {
        path: 'search',
        loadChildren: './search/search.module#SearchModule'
    },
    {
        path: 'category',
        loadChildren: './category/category.module#CategoryModule'
    },
    {
        path: '',
        loadChildren: './home/home.module#HomeModule'
    },
    {
        path: '**',
        loadChildren: './not-found/not-found.module#NotFoundModule'
    }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
