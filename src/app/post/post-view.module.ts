import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { PostViewRoutingModule } from './post-view-routing.module';
import { PostViewComponent } from './post-view.component';
import { RouterModule } from '@angular/router';
import { ShareModule } from '@ngx-share/core';
import { ShareButtonsModule } from '@ngx-share/buttons';
import { MatChipsModule } from '@angular/material';
import { PostFormComponent } from './post-form/post-form.component';
import { NgxUiLoaderModule } from 'ngx-ui-loader';



@NgModule({
  declarations: [PostViewComponent, PostFormComponent],
  imports: [
    CommonModule,
    PostViewRoutingModule,
    SharedModule,
    RouterModule,
    ShareButtonsModule,
    MatChipsModule,
    NgxUiLoaderModule
  ]
})
export class PostViewModule { }
