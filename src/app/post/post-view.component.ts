import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Post } from '../models/post.model';
import { PostService } from '../shared/service/post.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Title, Meta } from '@angular/platform-browser';
import { PostTagService } from '../shared/service/post-tag.service';

@Component({
  selector: 'app-post-view',
  templateUrl: './post-view.component.html',
  styleUrls: ['./post-view.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class PostViewComponent implements OnInit {

    postId: number;
    post: Post;

    constructor(
        private postService: PostService,
        private activatedRoute: ActivatedRoute,
        private postTagService: PostTagService,
        private router: Router
        ) { }

    ngOnInit() {
        this.postId = parseInt(this.activatedRoute.snapshot.paramMap.get('id'));
        this.getPost();
    }

    private getPost() {
        this.postService.getPost(this.postId).subscribe(response => {
            this.post = response;
            this.postTagService.setTitle(this.getPostTitle(response));
            this.postTagService.setSocialMediaTags(
                this.router.url,
                this.getPostTitle(response),
                this.getPostSubtitle(response),
                this.getImageUrl(response)
            );
        });
    }

    getImageUrl(post: Post) {
        if (post.postText) {
            return post.postText.image.url;
        }
        if (post.postForm) {
            return post.postForm.image.url;
        }
    }

    getPostTitle(post: Post) {
        if (post.postText) {
            return post.postText.title;
        }
        if (post.postForm) {
            return post.postForm.title;
        }
    }

    getPostSubtitle(post: Post) {
        if (post.postText) {
            return post.postText.subtitle;
        }
        if (post.postForm) {
            return post.postForm.subtitle;
        }
    }

}
