import { Component, OnInit, Input } from '@angular/core';
import { PostForm } from 'src/app/models/post-form.model';
import { FormGroup, FormBuilder, FormArray, Validators } from '@angular/forms';

@Component({
  selector: 'app-post-form',
  templateUrl: './post-form.component.html',
  styleUrls: ['./post-form.component.scss']
})
export class PostFormComponent implements OnInit {

    @Input() postForm: PostForm;
    form: FormGroup;
    answersForm: FormArray;

    constructor(
        private fb: FormBuilder
    ) { }

    ngOnInit() {
        this.createForm();
    }

    private createForm() {
        this.form = this.fb.group({
            id: this.fb.control(this.postForm.id, [Validators.required]),
            answers: this.fb.array([])
        });

        this.answersForm = this.form.get('answers') as FormArray;
        this.postForm.questions.forEach(question => {
            this.answersForm.push(
                this.fb.group({
                    question: this.fb.control(question, [Validators.required]),
                    answer: this.fb.control(null, [Validators.required])
                })
            );
        });
    }

}
