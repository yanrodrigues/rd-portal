import { Component, OnInit } from '@angular/core';
import { Pageable } from '../models/pageable.model';
import { Post } from '../models/post.model';
import { PostService } from '../shared/service/post.service';
import { ActivatedRoute } from '@angular/router';
import { Category } from '../models/category.model';
import { CategoryService } from '../shared/service/category.service';
import { IndexComponent } from '../models/index-component.model';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {

    paging: Pageable = new Pageable(0, 4, 'id');
    posts: Post[] = [];
    loading: boolean;
    showMore: boolean;
    categoryId: number;
    categoryIndex: IndexComponent;

    constructor(
        private postService: PostService,
        private categoryService: CategoryService,
        private route: ActivatedRoute
    ) { }

    ngOnInit() {
        this.route.params.subscribe(param => {
            this.categoryId = param.id;
            this.posts = [];
            this.paging.pageNo = 0;
            this.getCategory();
            this.getPosts();
        });
    }

    private getCategory() {
        this.categoryService.getCategory(this.categoryId).subscribe(response => {
            this.categoryIndex = new IndexComponent('highlight', response, 0, true);
        });
    }

    getPosts(addedPage?: boolean) {
        if (addedPage === true) {
            this.paging.pageNo = this.paging.pageNo + 1;
        }
        this.loading = true;
        this.postService.getPostsByCategoryWithPagination(this.paging, this.categoryId)
        .subscribe((posts: Post[]) => {
            this.showMore = !!posts.length;
            if (posts.length) {
                posts.forEach(post => this.posts.push(post));
            }
            this.loading = false;
        });
    }

}
