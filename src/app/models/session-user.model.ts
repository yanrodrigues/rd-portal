import { Role } from './role.enum';
import { StorageFile } from './storage-file.model';
import { Category } from './category.model';

export class SessionUser {
    constructor(
      public id: number,
      public username: string,
      public password: string,
      public dateCreated: Date,
      public authorities: Role[],
      public nomeCompleto: string,
      public email: string,
      public image: StorageFile,
      public categories: Category[],
      public publicName: string,
      public publicDescription: string
    ) { }
}
