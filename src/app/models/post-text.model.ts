import { StorageFile } from './storage-file.model';

export class PostText {
    constructor(
      public id: number,
      public title: string,
      public subtitle: string,
      public content: string,
      public image: StorageFile
    ) { }
}
