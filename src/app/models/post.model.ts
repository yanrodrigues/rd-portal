import { Category } from './category.model';
import { PostText } from './post-text.model';
import { PostForm } from './post-form.model';
import { SessionUser } from './session-user.model';

export class Post {
    constructor(
      public id: number,
      public postText: PostText,
      public postForm: PostForm,
      public categories: Category[],
      public dateCreated: Date,
      public dateUpdated: Date,
      public createdBy: SessionUser
    ) { }
}
