import { Role } from './role.enum';

export class Jwt {
    constructor(
      public sub: string,
      public exp: any,
      public roles: string
    ) { }
}
