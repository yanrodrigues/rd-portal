import { StorageFile } from './storage-file.model';

export class PostForm {
    constructor(
      public id: number,
      public title: string,
      public subtitle: string,
      public questions: string[],
      public image: StorageFile
    ) { }
}
