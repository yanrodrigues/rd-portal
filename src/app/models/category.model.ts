import { StorageFile } from './storage-file.model';

export class Category {
    constructor(
      public id: number,
      public name: string,
      public description: string,
      public image: StorageFile
    ) { }
}
