export class Pageable {
    constructor(
        public pageNo: number,
        public pageSize: number,
        public sortBy: string
    ) { }
}
