import { Category } from './category.model';

export class Menu {
    constructor(
        public category: Category,
        public zindex: number,
        public visibility: boolean
    ) { }
}
