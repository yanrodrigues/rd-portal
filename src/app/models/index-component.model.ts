import { Category } from './category.model';

export class IndexComponent {
    constructor(
        public type: string,
        public category: Category,
        public zindex: number,
        public visibility: boolean
    ) { }
}
