export class StorageFile {
    constructor(
      public id: number,
      public url: string
    ) { }
}
