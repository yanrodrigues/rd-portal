import { Post } from './post.model';

export class Slide {
    constructor(
        public post: Post,
        public zindex: number,
        public visibility: boolean
    ) { }
}
