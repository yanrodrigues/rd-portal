import { Injectable, Inject, PLATFORM_ID } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { JwtHelperService } from "@auth0/angular-jwt";
import { SessionUser } from 'src/app/models/session-user.model';
import { tap, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { Jwt } from 'src/app/models/jwt.model';
import { Role } from 'src/app/models/role.enum';
import { NgxPermissionsService } from 'ngx-permissions';
import { isPlatformBrowser } from '@angular/common';

const helper = new JwtHelperService();

const tokenKey = environment.token_session;

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
      private http: HttpClient,
      private router: Router,
      private permissionsService: NgxPermissionsService,
      @Inject(PLATFORM_ID) private platformID: Object
    ) { }

  signIn(username: string, password: string): Observable<any> {
    return this.http.post(`login`, {username, password}, {observe: 'response'});
  }

  getUser(): Observable<SessionUser> {
      if (!isPlatformBrowser(this.platformID)) {
          return of(null);
      }
      const user = JSON.parse(localStorage.getItem('user'));
      const jwt: Jwt = helper.decodeToken(localStorage.getItem(tokenKey));
      if (!user) {
          if (!jwt) {
            return of(null);
          }
          return this.http.get('user?username=' + jwt.sub).pipe(
            tap((userDetail: SessionUser) => {
                userDetail.authorities = jwt.roles.split(',') as Role[];
                localStorage.setItem('user', JSON.stringify(userDetail));
            }),
            catchError(error => {
                return of(null);
            }));
      } else {
          return of(user);
      }
  }

  updateUser(): Observable<SessionUser> {
      if (!isPlatformBrowser(this.platformID)) {
          return of(null);
      }
      const jwt: Jwt = helper.decodeToken(localStorage.getItem(tokenKey));
      if (!jwt) {
        return of(null);
      }
      return this.http.get('user?username=' + jwt.sub).pipe(
        tap((userDetail: SessionUser) => {
            userDetail.authorities = jwt.roles.split(',') as Role[];
            localStorage.setItem('user', JSON.stringify(userDetail));
        }),
        catchError(error => {
            return of(null);
        }));
  }

    logout() {
        if (isPlatformBrowser(this.platformID)) {
            localStorage.clear();
        }
        this.permissionsService.flushPermissions();
        this.router.navigate(['login']);
    }
}
