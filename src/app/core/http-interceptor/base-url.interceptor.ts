import { Injectable, PLATFORM_ID, Inject } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';
import { isPlatformBrowser } from '@angular/common';
import { ActivatedRoute } from '@angular/router';

const ignoredPaths = ['assets/i18n/pt_br.json'];
const dashboardPath = 'dashboard';
const loginPath = 'login';
const tokenKey = environment.token_session;

@Injectable()
export class BaseUrlInterceptor implements HttpInterceptor {

    private baseUrl: string = environment.BASE_URL;

    constructor(
        @Inject(PLATFORM_ID) private platformID: Object,
        private activatedRoute: ActivatedRoute
    ) {
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (!ignoredPaths.includes(request.url)) {
            let baseUrlRequest = request.clone({ url: `${this.baseUrl}${request.url}` });
            const originUrl = this.activatedRoute.snapshot['_routerState'].url;

            if (isPlatformBrowser(this.platformID) &&
                (originUrl.includes(dashboardPath) ||
                originUrl.includes(loginPath))) {
                const token = localStorage.getItem(tokenKey);
                if (token) {
                    baseUrlRequest = baseUrlRequest.clone({ setHeaders: { Authorization: token} });
                }
            }
            return next.handle(baseUrlRequest);
        }
        return next.handle(request);
    }
}
