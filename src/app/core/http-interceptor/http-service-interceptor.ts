import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from '../auth/auth.service';

@Injectable()
export class HttpServiceInterceptor implements HttpInterceptor {
    constructor(
        private router: Router,
        private messageService: ToastrService,
        private auth: AuthService
    ) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(req).pipe(

            tap((event: HttpEvent<any>) => {
            }),

            catchError(error => {
                if (error instanceof HttpErrorResponse) {
                    if (error.status === 403) {
                        this.auth.logout();
                    }

                    if (error.status === 500) {
                        this.messageService.error('Ocorreu um erro no sistema, tente novamente ou contate o suporte técnico.');
                        return;
                    }
                }
                return throwError(error);
            })
        );
    }
}
