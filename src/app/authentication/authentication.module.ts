import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthenticationRoutingModule } from './authentication-routing.module';
import { AuthenticationLoginComponent } from './authentication-login/authentication-login.component';
import { SharedModule } from '../shared/shared.module';
import { LoginGuard } from '../core/auth/login.guard';
import { NgxUiLoaderModule } from 'ngx-ui-loader';


@NgModule({
  declarations: [AuthenticationLoginComponent],
  imports: [
    CommonModule,
    AuthenticationRoutingModule,
    SharedModule,
    NgxUiLoaderModule
  ],
  providers: [
      LoginGuard
  ]
})
export class AuthenticationModule { }
