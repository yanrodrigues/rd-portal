import { Component, OnInit, Inject, PLATFORM_ID } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/core/auth/auth.service';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';
import { isPlatformBrowser } from '@angular/common';

const token_key = environment.token_session;

@Component({
  selector: 'app-authentication-login',
  templateUrl: './authentication-login.component.html',
  styleUrls: ['./authentication-login.component.scss']
})
export class AuthenticationLoginComponent implements OnInit {

    loginForm: FormGroup;
    platform;

    constructor(
        private messageService: ToastrService,
        private authService: AuthService,
        private fb: FormBuilder,
        private router: Router,
        @Inject(PLATFORM_ID) platformId: string
    ) {
        this.platform = platformId;
    }

    ngOnInit() {
        if (isPlatformBrowser(this.platform)) {
            this.authService.getUser().subscribe(res => {
                if (res !== null) {
                    this.router.navigate(['dashboard']);
                }
            });
        }
        this.createForm();
    }

    private createForm() {
        this.loginForm = this.fb.group({
            username: this.fb.control(null, [Validators.required, Validators.minLength(3)]),
            password: this.fb.control(null, [Validators.required, Validators.minLength(3)])
        });
    }

    signIn() {
        if (this.loginForm.invalid) {
            this.messageService.error('Preencha todos os campos corretamente.');
            return;
        }

        const form = this.loginForm.getRawValue();

        this.authService.signIn(form.username, form.password)
            .subscribe((res: Response) => {
                localStorage.setItem(token_key, res.headers.get('Authorization'));
                this.messageService.success('Logado com sucesso!');
                this.router.navigate(['dashboard']);
            }, error => {
                this.messageService.error('Credenciais inválida!');
                this.loginForm.reset();
            });
    }

}
