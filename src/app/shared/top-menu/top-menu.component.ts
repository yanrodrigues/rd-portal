import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { Category } from 'src/app/models/category.model';
import { MenuService } from '../service/menu.service';
import { Menu } from 'src/app/models/menu.model';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-top-menu',
  templateUrl: './top-menu.component.html',
  styleUrls: ['./top-menu.component.scss']
})
export class TopMenuComponent implements OnInit {

    menu: Menu[] = [];
    searchStatus: boolean;
    searchControl: FormControl = this.fb.control(null, [Validators.required]);
    toggleMobile = false;

    @Output() searched = new EventEmitter<void>();

    constructor(
        private menuSerivce: MenuService,
        private fb: FormBuilder,
        private router: Router
    ) { }

    ngOnInit() {
        this.getCategories();
    }

    private getCategories() {
        this.menuSerivce.getAllMenu().subscribe(response => {
            this.menu = response.sort(this.sortMenu);
        });
    }

    private sortMenu(a: Menu, b: Menu) {
        if (a.zindex > b.zindex) {
            return 1;
        }
        if (b.zindex > a.zindex) {
            return -1;
        }
        return 0;
    }

    onSubmit() {
        if (this.searchControl.valid) {
            const searchString = this.searchControl.value;
            this.searchControl.setValue(null);
            this.router.navigate(['search'], {queryParams: {query: searchString}});
            this.searched.emit();
            this.closeSearch();
        }
    }

    openSearch() {
        this.searchStatus = true;
    }

    closeSearch() {
        this.searchStatus = false;
    }

    toggleMobileOptions() {
        this.toggleMobile = !this.toggleMobile;
    }

}
