import { Component, OnInit, Input, Inject, PLATFORM_ID } from '@angular/core';
import { Slide } from 'src/app/models/slide.model';
import { trigger, transition, style, animate } from '@angular/animations';
import { isPlatformBrowser } from '@angular/common';
import { interval, Subject, timer } from 'rxjs';
import { switchMap, takeUntil, repeatWhen } from 'rxjs/operators';

@Component({
  selector: 'app-slide-carousel',
  templateUrl: './slide-carousel.component.html',
  styleUrls: ['./slide-carousel.component.scss'],
  animations: [
    trigger(
      'enterAnimation', [
        transition(':enter', [
          style({ opacity: 0}),
          animate('250ms', style({ opacity: 1}))
        ]),
        transition(':leave', [
          style({ opacity: 1}),
          animate('250ms', style({ opacity: 0}))
        ])
      ]
    )
  ],
})
export class SlideCarouselComponent implements OnInit {

    @Input() slides: Slide[] = [];

    private platform;

    slideCounter: number;
    private slideInterval;
    private stopInterval: Subject<void> = new Subject<void>();
    private startInterval: Subject<void> = new Subject<void>();

    constructor(
        @Inject(PLATFORM_ID) platformId: string
    ) {
        this.platform = platformId;
    }

    ngOnInit() {
        this.startCounter();
        this.startSlideInterval();
    }

    private startCounter() {
        this.slideCounter = 0;
    }

    startSlideInterval() {
        this.slideInterval = timer(4000)
            .pipe(
                takeUntil(this.stopInterval),
                repeatWhen(() => this.startInterval)
            ).subscribe(() => {
                this.nextSlide();
            });
    }

    getSlideImageUrl(slide: Slide) {
        if (!!slide.post.postText) {
            return slide.post.postText.image.url;
        }
        if (!!slide.post.postForm) {
            return slide.post.postForm.image.url;
        }
    }

    getSlideContentTitle(slide: Slide) {
        if (!!slide.post.postText) {
            return slide.post.postText.title;
        }
        if (!!slide.post.postForm) {
            return slide.post.postForm.title;
        }
    }

    nextSlide() {
        this.stopInterval.next();

        if (this.slides && ((this.slideCounter + 1) === this.slides.length)) {
            this.slideCounter = 0;
            this.startInterval.next();
            return;
        }
        this.slideCounter = this.slideCounter + 1;

        this.startInterval.next();
    }

    prevSlide() {
        this.stopInterval.next();

        if (this.slideCounter === 0) {
            this.slideCounter = this.slides.length - 1;
            this.startInterval.next();
            return;
        }
        this.slideCounter = this.slideCounter - 1;

        this.startInterval.next();
    }

}
