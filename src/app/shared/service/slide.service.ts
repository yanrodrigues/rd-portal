import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Slide } from 'src/app/models/slide.model';

@Injectable({
  providedIn: 'root'
})
export class SlideService {

    constructor(
        private http: HttpClient,
    ) { }

    saveSlides(slides: Slide[]): Observable<any> {
        return this.http.post(`slide/register`, slides);
    }

    getAllSlides(): Observable<any> {
        return this.http.get(`slide`);
    }

    updateSlides(slides: Slide[]): Observable<any> {
        return this.http.put(`slide`, slides);
    }

}
