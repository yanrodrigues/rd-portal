import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

    constructor(
        private http: HttpClient,
    ) { }

    saveCategory(form): Observable<any> {
        return this.http.post(`category/register`, form);
    }

    updateCategory(form): Observable<any> {
        return this.http.put(`category`, form);
    }

    getAllCategory(): Observable<any> {
        return this.http.get(`category/all`);
    }

    getCategory(id: number): Observable<any> {
        return this.http.get(`category/${id}`);
    }

    deleteCategory(id: number): Observable<any> {
        return this.http.delete(`category/${id}`);
    }

}
