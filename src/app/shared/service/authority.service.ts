import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthorityService {

    constructor(
        private http: HttpClient,
    ) { }

    getAuthorities(): Observable<any> {
        return this.http.get(`authorities`);
    }

}
