import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IndexComponent } from 'src/app/models/index-component.model';

@Injectable({
  providedIn: 'root'
})
export class IndexComponentService {

    constructor(
        private http: HttpClient,
    ) { }

    saveIndexComponents(indexComponent: IndexComponent[]): Observable<any> {
        return this.http.post(`index/register`, indexComponent);
    }

    getAllIndexComponents(): Observable<any> {
        return this.http.get(`index`);
    }

    updateIndexComponents(indexComponent: IndexComponent[]): Observable<any> {
        return this.http.put(`index`, indexComponent);
    }

}
