import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Pageable } from 'src/app/models/pageable.model';
import { Category } from 'src/app/models/category.model';

@Injectable({
  providedIn: 'root'
})
export class PostService {

    constructor(
        private http: HttpClient,
    ) { }

    savePost(form): Observable<any> {
        return this.http.post(`post/register`, form);
    }

    getAllPost(): Observable<any> {
        return this.http.get(`post`);
    }

    getAllPostWithPagination(paging: Pageable): Observable<any> {
        return this.http.get(`post/pages?pageNo=${paging.pageNo}&pageSize=${paging.pageSize}&sortBy=${paging.sortBy}`);
    }

    getPostsByCategoryWithPagination(paging: Pageable, id: number): Observable<any> {
        return this.http.get(`post/category/${id}/pages?pageNo=${paging.pageNo}&pageSize=${paging.pageSize}&sortBy=${paging.sortBy}`);
    }

    getPostsBySearchWithPagination(paging: Pageable, search: string): Observable<any> {
        return this.http.get(`post/search/pages?pageNo=${paging.pageNo}&pageSize=${paging.pageSize}&sortBy=${paging.sortBy}&search=${search}`);
    }

    getPost(id: number): Observable<any> {
        return this.http.get(`post/${id}`);
    }

    updatePost(form): Observable<any> {
        return this.http.put(`post`, form);
    }

    deletePost(id: number): Observable<any> {
        return this.http.delete(`post/${id}`);
    }

}
