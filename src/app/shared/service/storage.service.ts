import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { JwtHelperService } from "@auth0/angular-jwt";
import { SessionUser } from 'src/app/models/session-user.model';
import { tap, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { Jwt } from 'src/app/models/jwt.model';
import { Role } from 'src/app/models/role.enum';
import { NgxPermissionsService } from 'ngx-permissions';

const helper = new JwtHelperService();

const tokenKey = environment.token_session;

@Injectable({
  providedIn: 'root'
})
export class StorageService {

    constructor(
        private http: HttpClient,
    ) { }

    uploadFile(formData: FormData): Observable<any> {
        return this.http.post(`storage/file`, formData);
    }

    deleteFile(id: number): Observable<any> {
        return this.http.delete(`storage/file/${id}`);
    }

}
