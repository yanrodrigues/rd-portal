import { Injectable } from '@angular/core';
import { Title, Meta } from '@angular/platform-browser';
import { MetaTag } from 'src/app/models/meta-tag.model';

@Injectable({
  providedIn: 'root'
})
export class PostTagService {
    private urlMeta: string = "og:url";
    private titleMeta: string = "og:title";
    private descriptionMeta: string = "og:description";
    private imageMeta: string = "og:image";
    private secureImageMeta: string = "og:image:secure_url";
    private twitterTitleMeta: string = "twitter:text:title";
    private twitterImageMeta: string = "twitter:image";

    constructor(private titleService: Title, private metaService: Meta) { }

    public setTitle(title: string): void {
      this.titleService.setTitle(title);
    }

    public setSocialMediaTags(url: string, title: string, description: string, image: string): void {
        this.titleService.setTitle(title);
        this.metaService.updateTag({property: 'og:title', content: title});
        this.metaService.updateTag({property: 'og:url', content: url});
        this.metaService.updateTag({property: 'og:description', content: description});
        this.metaService.updateTag({property: 'og:image', content: image});
    }
}
