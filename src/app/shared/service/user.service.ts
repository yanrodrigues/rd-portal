import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

    constructor(
        private http: HttpClient,
    ) { }

    saveUser(form): Observable<any> {
        return this.http.post(`register`, form);
    }

    updateUser(form): Observable<any> {
        return this.http.put(`users`, form);
    }

    getAllUsers(): Observable<any> {
        return this.http.get(`users`);
    }

    getUser(id: number): Observable<any> {
        return this.http.get(`users/${id}`);
    }

    deleteUser(id: number): Observable<any> {
        return this.http.delete(`users/${id}`);
    }

}
