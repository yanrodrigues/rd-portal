import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Menu } from 'src/app/models/menu.model';

@Injectable({
  providedIn: 'root'
})
export class MenuService {

    constructor(
        private http: HttpClient,
    ) { }

    saveMenu(menu: Menu[]): Observable<any> {
        return this.http.post(`menu/register`, menu);
    }

    getAllMenu(): Observable<any> {
        return this.http.get(`menu`);
    }

    updateMenu(menu: Menu[]): Observable<any> {
        return this.http.put(`menu`, menu);
    }

}
