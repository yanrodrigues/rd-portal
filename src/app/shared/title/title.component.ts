import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-title',
  templateUrl: './title.component.html',
  styleUrls: ['./title.component.scss']
})
export class TitleComponent implements OnInit {

    @Input() section: string;
    @Input() page: string;
    @Input() sectionUrl: string = '';

  constructor() { }

  ngOnInit() {
  }

}
