import { Component, OnInit } from '@angular/core';
import { Post } from 'src/app/models/post.model';
import { PostService } from '../service/post.service';
import { Pageable } from 'src/app/models/pageable.model';

@Component({
  selector: 'app-general-post-list',
  templateUrl: './general-post-list.component.html',
  styleUrls: ['./general-post-list.component.scss']
})
export class GeneralPostListComponent implements OnInit {

    posts: Post[] = [];

    loading: boolean;
    paging: Pageable = new Pageable(0, 4, 'id');

    constructor(
        private postService: PostService
    ) { }

    ngOnInit() {
        this.getPosts();
    }

    getPosts(addedPage?: boolean) {
        if (addedPage === true) {
            this.paging.pageNo = this.paging.pageNo + 1;
        }
        this.loading = true;
        this.postService.getAllPostWithPagination(
            this.paging
        ).subscribe((posts: Post[]) => {
            if (posts.length) {
                posts.forEach(post => this.posts.push(post));
            }
            this.loading = false;
        });
    }

    getImageUrl(post: Post): string {
        if (!!post.postText) {
            return post.postText.image ? post.postText.image.url : 'https://image.flaticon.com/icons/svg/21/21104.svg';
        }

        if (!!post.postForm) {
            return post.postForm.image ? post.postForm.image.url : 'https://image.flaticon.com/icons/svg/21/21104.svg';
        }
    }

    getPostTitle(post: Post): string {
        if (!!post.postText) {
            return post.postText.title;
        }

        if (!! post.postForm) {
            return post.postForm.title;
        }
    }

}
