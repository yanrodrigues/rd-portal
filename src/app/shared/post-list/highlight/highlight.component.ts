import { Component, OnInit, Input } from '@angular/core';
import { IndexComponent } from 'src/app/models/index-component.model';

@Component({
  selector: 'app-highlight',
  templateUrl: './highlight.component.html',
  styleUrls: ['./highlight.component.scss']
})
export class HighlightComponent implements OnInit {

    @Input() component: IndexComponent;
    @Input() showMore = true;

    constructor() { }

    ngOnInit() {
    }

}
