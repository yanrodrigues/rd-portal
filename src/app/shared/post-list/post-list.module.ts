import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ComponentTextComponent } from './component-text/component-text.component';
import { HighlightComponent } from './highlight/highlight.component';
import { MaterialModule } from '../material/material.module';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [ComponentTextComponent, HighlightComponent],
  imports: [
    CommonModule,
    RouterModule,
    MaterialModule
  ],
  exports: [
      ComponentTextComponent,
      HighlightComponent
  ]
})
export class PostListModule { }
