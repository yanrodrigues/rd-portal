import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComponentTextComponent } from './component-text.component';

describe('ComponentTextComponent', () => {
  let component: ComponentTextComponent;
  let fixture: ComponentFixture<ComponentTextComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComponentTextComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComponentTextComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
