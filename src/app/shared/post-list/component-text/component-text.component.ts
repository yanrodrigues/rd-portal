import { Component, OnInit, Input } from '@angular/core';
import { IndexComponent } from 'src/app/models/index-component.model';
import { Post } from 'src/app/models/post.model';
import { PostService } from '../../service/post.service';
import { Pageable } from 'src/app/models/pageable.model';

@Component({
  selector: 'app-component-text',
  templateUrl: './component-text.component.html',
  styleUrls: ['./component-text.component.scss']
})
export class ComponentTextComponent implements OnInit {

    @Input() component: IndexComponent;

    paging: Pageable = new Pageable(0, 4, 'id');
    posts: Post[];

    constructor(
        private postService: PostService
    ) { }

    ngOnInit() {
        this.getPosts();
    }

    private getPosts() {
        this.postService.getPostsByCategoryWithPagination(
            this.paging,
            this.component.category.id
        ).subscribe(posts => {
            this.posts = posts;
        });
    }

}
