import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { StorageService } from '../service/storage.service';

@Component({
  selector: 'app-image-uploader',
  templateUrl: './image-uploader.component.html',
  styleUrls: ['./image-uploader.component.scss']
})
export class ImageUploaderComponent implements OnInit {

    @Input() imageControl: FormControl = this.fb.control(null);
    @Input() isEdit = false;
    imageUrl: string;

    constructor(
        private fb: FormBuilder,
        private storageService: StorageService
    ) { }

    ngOnInit() {
        if (this.imageControl.value) {
            this.imageUrl = this.imageControl.value.url;
        }
    }

    imageChange(file) {
        const formData = new FormData();
        formData.append('file', file);

        this.storageService.uploadFile(formData).subscribe(res => {
            this.imageControl.setValue(res);
            this.imageUrl = res.url;
        });
    }

    deleteImage() {
        this.imageControl.setValue(null);
        this.imageUrl = null;
    }
}
