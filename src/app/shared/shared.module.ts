import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from './material/material.module';
import { NgxPermissionsModule } from 'ngx-permissions';
import { TitleComponent } from './title/title.component';
import { MatChipsModule, MatCardModule, MatDialogModule } from '@angular/material';
import { RouterModule } from '@angular/router';
import { ActionListComponent } from './action-list/action-list.component';
import { ImageUploaderComponent } from './image-uploader/image-uploader.component';
import { ConfirmModalComponent } from './confirm-modal/confirm-modal.component';
import { TopMenuComponent } from './top-menu/top-menu.component';
import { SlideCarouselComponent } from './slide-carousel/slide-carousel.component';
import { PostListModule } from './post-list/post-list.module';
import { FooterComponent } from './footer/footer.component';
import { GeneralPostListComponent } from './general-post-list/general-post-list.component';



@NgModule({
  declarations: [
      TitleComponent,
      ActionListComponent,
      ImageUploaderComponent,
      ConfirmModalComponent,
      TopMenuComponent,
      SlideCarouselComponent,
      FooterComponent,
      GeneralPostListComponent
    ],
  imports: [
    CommonModule,
    FormsModule,
    MaterialModule,
    ReactiveFormsModule,
    NgxPermissionsModule,
    MatChipsModule,
    RouterModule,
    MatCardModule,
    MatDialogModule,
    PostListModule
  ],
  exports: [
      FormsModule,
      MaterialModule,
      ReactiveFormsModule,
      NgxPermissionsModule,
      TitleComponent,
      MatChipsModule,
      ActionListComponent,
      MatCardModule,
      ImageUploaderComponent,
      MatDialogModule,
      ConfirmModalComponent,
      TopMenuComponent,
      SlideCarouselComponent,
      PostListModule,
      FooterComponent,
      GeneralPostListComponent
  ],
  entryComponents: [
      ConfirmModalComponent
  ]
})
export class SharedModule { }
