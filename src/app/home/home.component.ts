import { Component, OnInit } from '@angular/core';
import { Slide } from '../models/slide.model';
import { SlideService } from '../shared/service/slide.service';
import { IndexComponent } from '../models/index-component.model';
import { IndexComponentService } from '../shared/service/index-component.service';
import { PostTagService } from '../shared/service/post-tag.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

    slidePosts: Slide[] = [];

    indexComponents: IndexComponent[] = [];

    constructor(
        private slideService: SlideService,
        private indexService: IndexComponentService,
        private postTagService: PostTagService
    ) { }

    ngOnInit() {
        this.getSlides();
        this.getIndexComponents();
        this.postTagService.setSocialMediaTags(
            'http://rd-portal.herokuapp.com',
            'rdPortal',
            'Portal da rd',
            'https://cdn.lynda.com/course/82409/82409-636282992381780999-16x9.jpg'
        );
    }

    private getSlides() {
        this.slideService.getAllSlides().subscribe(response => {
            this.slidePosts = response;
        });
    }

    private getIndexComponents() {
        this.indexService.getAllIndexComponents().subscribe(response => {
            this.indexComponents = response;
        });
    }

}
