import { Component, OnInit } from '@angular/core';
import { SlideService } from '../shared/service/slide.service';
import { Slide } from '../models/slide.model';
import { Pageable } from '../models/pageable.model';
import { PostService } from '../shared/service/post.service';
import { Post } from '../models/post.model';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

    slidePosts: Slide[] = [];

    paging: Pageable = new Pageable(0, 4, 'id');
    query = this.route.snapshot.queryParams.query;
    posts: Post[] = [];
    loading: boolean;
    showMore: boolean;

    constructor(
        private slideService: SlideService,
        private postService: PostService,
        private route: ActivatedRoute
    ) { }

    ngOnInit() {
        this.route.queryParams.subscribe(params => {
            this.posts = [];
            this.query = params.query;
            this.paging.pageNo = 0;
            this.getPosts();
        });

        this.getSlides();
    }

    private getSlides() {
        this.slideService.getAllSlides().subscribe(response => {
            this.slidePosts = response;
        });
    }

    getPosts(addedPage?: boolean) {
        if (addedPage === true) {
            this.paging.pageNo = this.paging.pageNo + 1;
        }
        this.loading = true;
        this.postService.getPostsBySearchWithPagination(this.paging, this.query)
        .subscribe((posts: Post[]) => {
            this.showMore = !!posts.length;
            if (posts.length) {
                posts.forEach(post => this.posts.push(post));
            }
            this.loading = false;
        });
    }

}
