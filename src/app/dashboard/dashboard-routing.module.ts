import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardLayoutComponent } from './dashboard-layout/dashboard-layout.component';
import { PermissionsGuard } from '../core/auth/permissions.guard';

const routes: Routes = [
    {
        path: '',
        component: DashboardLayoutComponent,
        children: [
            {
                path: 'users',
                loadChildren: './users/users.module#UsersModule'
            },
            {
                path: 'categories',
                loadChildren: './categories/categories.module#CategoriesModule'
            },
            {
                path: 'post',
                loadChildren: './post/post.module#PostModule'
            },
            {
                path: 'index',
                loadChildren: './index/index.module#IndexModule'
            },
            {
                path: 'slide',
                loadChildren: './slide/slide.module#SlideModule'
            },
            {
                path: 'menu',
                loadChildren: './menu/menu.module#MenuModule'
            },
            {
                path: 'profile',
                loadChildren: './profile/profile.module#ProfileModule'
            }
        ]
    },
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
