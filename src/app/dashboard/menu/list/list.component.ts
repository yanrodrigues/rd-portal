import { Component, OnInit } from '@angular/core';
import { Menu } from 'src/app/models/menu.model';
import { Category } from 'src/app/models/category.model';
import { MenuService } from 'src/app/shared/service/menu.service';
import { CategoryService } from 'src/app/shared/service/category.service';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

    isEdit: boolean;
    categories: Category[];
    menu: Menu[] = [];

    constructor(
        private menuService: MenuService,
        private categoryService: CategoryService,
        private messageService: ToastrService
    ) { }

    ngOnInit() {
        this.getMenu();
        this.getCategories();
    }

    drop(event: CdkDragDrop<Menu[]>) {
        moveItemInArray(this.menu, event.previousIndex, event.currentIndex);
        this.setMenuOrder();
    }

    addMenu(menu: Menu) {
        this.menu.push(menu);
    }

    deleteMenu(index: number) {
        this.menu.splice(index, 1);
    }

    setMenuOrder() {
        for (let i = 0; i < this.menu.length; i++) {
            this.menu[i].zindex = i;
        }
    }

    saveMenu() {
        this.setMenuOrder();

        if (this.isEdit) {
            this.menuService.updateMenu(this.menu).subscribe(res => {
                this.messageService.success('Menu editado com sucesso!');
            }, error => {
                this.messageService.error('Erro para editar o menu, tente novamente ou entre em contato com o suporte.');
            });
        } else {
            this.menuService.saveMenu(this.menu).subscribe(res => {
                this.messageService.success('Menu salvo com sucesso!');
            }, error => {
                this.messageService.error('Erro para salvar o menu, tente novamente ou entre em contato com o suporte.');
            });
        }
    }

    private getCategories() {
        this.categoryService.getAllCategory().subscribe(response => {
            this.categories = response;
        });
    }

    private getMenu() {
        this.menuService.getAllMenu().subscribe(response => {
            if (!!response.length) {
                this.menu = response.sort(this.sortMenu);
                this.isEdit = true;
            } else {
                this.isEdit = false;
            }
        });
    }

    private sortMenu(a: Menu, b: Menu) {
        if (a.zindex > b.zindex) {
            return 1;
        }
        if (b.zindex > a.zindex) {
            return -1;
        }
        return 0;
      }

}
