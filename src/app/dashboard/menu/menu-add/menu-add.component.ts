import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Category } from 'src/app/models/category.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-menu-add',
  templateUrl: './menu-add.component.html',
  styleUrls: ['./menu-add.component.scss']
})
export class MenuAddComponent implements OnInit {

    @Input() categories: Category[];
    @Output() newMenu = new EventEmitter();

    addStatus = false;

    menuForm: FormGroup;

    constructor(
        private fb: FormBuilder,
        private messageService: ToastrService
    ) { }

    ngOnInit() {
    }

    private createForm() {
        this.menuForm = this.fb.group({
            category: this.fb.control(null, [Validators.required]),
            visibility: this.fb.control(true, [Validators.required])
        });
    }

    addMenuItem() {
        this.menuForm.markAllAsTouched();

        if (this.menuForm.invalid) {
            this.messageService.error('Preencha todos os campos obrigatórios para adicionar!');
            return;
        }

        this.newMenu.emit(this.menuForm.getRawValue());
        this.toggleAddStatus();
    }

    toggleAddStatus() {
        this.addStatus = !this.addStatus;

        if (this.addStatus) {
            this.createForm();
        } else {
            this.menuForm = null;
        }
    }

}
