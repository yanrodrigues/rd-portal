import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListComponent } from './list/list.component';
import { MenuAddComponent } from './menu-add/menu-add.component';
import { MenuRoutingModule } from './menu-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { DragDropModule } from '@angular/cdk/drag-drop';



@NgModule({
  declarations: [ListComponent, MenuAddComponent],
  imports: [
    CommonModule,
    MenuRoutingModule,
    SharedModule,
    DragDropModule
  ]
})
export class MenuModule { }
