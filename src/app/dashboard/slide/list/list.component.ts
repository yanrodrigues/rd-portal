import { Component, OnInit } from '@angular/core';
import { Post } from 'src/app/models/post.model';
import { PostService } from 'src/app/shared/service/post.service';
import { Slide } from 'src/app/models/slide.model';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { SlideService } from 'src/app/shared/service/slide.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

    isEdit: boolean;
    posts: Post[];
    slide: Slide[] = [];

    constructor(
        private postService: PostService,
        private slideService: SlideService,
        private messageService: ToastrService,
        private router: Router
    ) { }

    ngOnInit() {
        this.getSlides();
        this.getPosts();
    }

    drop(event: CdkDragDrop<Slide[]>) {
        moveItemInArray(this.slide, event.previousIndex, event.currentIndex);
        this.setSlideOrder();
    }

    addSlide(slide: Slide) {
        this.slide.push(slide);
    }

    deleteSlide(index: number) {
        this.slide.splice(index, 1);
    }

    setSlideOrder() {
        for (let i = 0; i < this.slide.length; i++) {
            this.slide[i].zindex = i;
        }
    }

    saveSlides() {
        this.setSlideOrder();

        if (this.isEdit) {
            this.slideService.updateSlides(this.slide).subscribe(res => {
                this.messageService.success('Slide editado com sucesso!');
            }, error => {
                this.messageService.error('Erro para editar o slide, tente novamente ou entre em contato com o suporte.');
            });
        } else {
            this.slideService.saveSlides(this.slide).subscribe(res => {
                this.messageService.success('Slide salvo com sucesso!');
            }, error => {
                this.messageService.error('Erro para salvar o slide, tente novamente ou entre em contato com o suporte.');
            });
        }
    }

    private getPosts() {
        this.postService.getAllPost().subscribe(response => {
            this.posts = response;
        });
    }

    private getSlides() {
        this.slideService.getAllSlides().subscribe(response => {
            if (!!response.length) {
                this.slide = response.sort(this.sortSlide);
                this.isEdit = true;
            } else {
                this.isEdit = false;
            }
        });
    }

    private sortSlide(a: Slide, b: Slide) {
        if (a.zindex > b.zindex) {
            return 1;
        }
        if (b.zindex > a.zindex) {
            return -1;
        }
        return 0;
      }

}
