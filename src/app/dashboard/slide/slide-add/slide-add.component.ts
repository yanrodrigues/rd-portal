import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Post } from 'src/app/models/post.model';

@Component({
  selector: 'app-slide-add',
  templateUrl: './slide-add.component.html',
  styleUrls: ['./slide-add.component.scss']
})
export class SlideAddComponent implements OnInit {

    @Input() posts: Post[];
    @Output() newSlide = new EventEmitter();

    addStatus = false;

    slideForm: FormGroup;

    constructor(
        private fb: FormBuilder,
        private messageService: ToastrService
    ) { }

    ngOnInit() {
    }

    private createForm() {
        this.slideForm = this.fb.group({
            post: this.fb.control(null, [Validators.required]),
            visibility: this.fb.control(true, [Validators.required])
        });
    }

    addComponent() {
        this.slideForm.markAllAsTouched();

        if (this.slideForm.invalid) {
            this.messageService.error('Preencha todos os campos obrigatórios para adicionar!');
            return;
        }

        this.newSlide.emit(this.slideForm.getRawValue());
        this.toggleAddStatus();
    }

    toggleAddStatus() {
        this.addStatus = !this.addStatus;

        if (this.addStatus) {
            this.createForm();
        } else {
            this.slideForm = null;
        }
    }

}
