import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListComponent } from './list/list.component';
import { SlideAddComponent } from './slide-add/slide-add.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { SlideRoutingModule } from './slide-routing.module';
import { DragDropModule } from '@angular/cdk/drag-drop';



@NgModule({
  declarations: [ListComponent, SlideAddComponent],
  imports: [
    CommonModule,
    SlideRoutingModule,
    SharedModule,
    DragDropModule
  ]
})
export class SlideModule { }
