import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { CategoryService } from 'src/app/shared/service/category.service';
import { Observable } from 'rxjs';
import { Category } from 'src/app/models/category.model';

@Component({
  selector: 'app-index-add',
  templateUrl: './index-add.component.html',
  styleUrls: ['./index-add.component.scss']
})
export class IndexAddComponent implements OnInit {

    @Input() categories: Category[];
    @Output() newComponent = new EventEmitter();

    addStatus = false;

    componentForm: FormGroup;

    constructor(
        private fb: FormBuilder,
        private messageService: ToastrService
    ) { }

    ngOnInit() {
    }

    private createForm() {
        this.componentForm = this.fb.group({
            type: this.fb.control(null, [Validators.required]),
            category: this.fb.control(null, [Validators.required]),
            visibility: this.fb.control(true, [Validators.required])
        });
    }

    addComponent() {
        this.componentForm.markAllAsTouched();

        if (this.componentForm.invalid) {
            this.messageService.error('Preencha todos os campos obrigatórios para adicionar!');
            return;
        }

        this.newComponent.emit(this.componentForm.getRawValue());
        this.toggleAddStatus();
    }

    toggleAddStatus() {
        this.addStatus = !this.addStatus;

        if (this.addStatus) {
            this.createForm();
        } else {
            this.componentForm = null;
        }
    }

}
