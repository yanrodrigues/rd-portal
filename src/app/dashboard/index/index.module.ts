import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { ListComponent } from './list/list.component';
import { IndexRoutingModule } from './index-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { IndexAddComponent } from './index-add/index-add.component';



@NgModule({
  declarations: [ListComponent, IndexAddComponent],
  imports: [
    CommonModule,
    IndexRoutingModule,
    SharedModule,
    DragDropModule
  ]
})
export class IndexModule { }
