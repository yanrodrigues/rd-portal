import { Component, OnInit } from '@angular/core';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { IndexComponent } from 'src/app/models/index-component.model';
import { Category } from 'src/app/models/category.model';
import { CategoryService } from 'src/app/shared/service/category.service';
import { IndexComponentService } from 'src/app/shared/service/index-component.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

    indexComponents: IndexComponent[] = [];
    isEdit: boolean;
    categories: Category[];

    constructor(
        private categoryService: CategoryService,
        private indexComponentService: IndexComponentService,
        private messageService: ToastrService,
        private router: Router
    ) { }

    ngOnInit() {
        this.getIndexComponents();
        this.getCategories();
    }

    drop(event: CdkDragDrop<IndexComponent[]>) {
        moveItemInArray(this.indexComponents, event.previousIndex, event.currentIndex);
        this.setIndexComponentOrder();
    }

    addComponent(component: IndexComponent) {
        this.indexComponents.push(component);
    }

    deleteComponent(index: number) {
        this.indexComponents.splice(index, 1);
    }

    saveComponentList() {
        this.setIndexComponentOrder();

        if (this.isEdit) {
            this.indexComponentService.updateIndexComponents(this.indexComponents).subscribe(res => {
                this.messageService.success('Index editada com sucesso!');
            }, error => {
                this.messageService.error('Erro para editar a index, tente novamente ou entre em contato com o suporte.');
            });
        } else {
            this.indexComponentService.saveIndexComponents(this.indexComponents).subscribe(res => {
                this.messageService.success('Index salva com sucesso!');
            }, error => {
                this.messageService.error('Erro para salvar a index, tente novamente ou entre em contato com o suporte.');
            });
        }
    }

    setIndexComponentOrder() {
        for (let i = 0; i < this.indexComponents.length; i++) {
            this.indexComponents[i].zindex = i;
        }
    }

    private getCategories() {
        this.categoryService.getAllCategory().subscribe(response => {
            this.categories = response;
        });
    }

    private getIndexComponents() {
        this.indexComponentService.getAllIndexComponents().subscribe(response => {
            if (!!response.length) {
                this.indexComponents = response.sort(this.sortListComponents);
                this.isEdit = true;
            } else {
                this.isEdit = false;
            }
        });
    }
    private sortListComponents(a: IndexComponent, b: IndexComponent) {
        if (a.zindex > b.zindex) {
            return 1;
        }
        if (b.zindex > a.zindex) {
            return -1;
        }
        return 0;
      }

}
