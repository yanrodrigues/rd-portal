import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CategoryService } from 'src/app/shared/service/category.service';
import { Category } from 'src/app/models/category.model';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

    category: Category;
    isEdit: boolean;
    categoryForm: FormGroup;

    constructor(
        private fb: FormBuilder,
        private categoryService: CategoryService,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private messageService: ToastrService
    ) { }

    ngOnInit() {
        const categoryId: number = parseInt(this.activatedRoute.snapshot.paramMap.get('id'));
        if (categoryId) {
            this.categoryService.getCategory(categoryId).subscribe(res => {
                if (res) {
                    this.isEdit = true;
                    this.category = res;
                    this.loadForm();
                } else {
                    this.router.navigate(['users']);
                }
            }, error => {
                this.router.navigate(['users']);
            });
        } else {
            this.isEdit = false;
            this.createForm();
        }
    }

    private createForm() {
        this.categoryForm = this.fb.group({
            name: this.fb.control(null, [Validators.required]),
            description: this.fb.control(null, [Validators.required]),
            image: this.fb.control(null, [Validators.required])
        });
    }


    private loadForm() {
        const category = this.category;

        this.categoryForm = this.fb.group({
            id: this.fb.control(category.id, [Validators.required]),
            name: this.fb.control(category.name, [Validators.required]),
            description: this.fb.control(category.description, [Validators.required]),
            image: this.fb.control(category.image, [Validators.required])
        });
    }

    onSubmit() {
        if (this.categoryForm.invalid) {
            this.messageService.error('Preencha todos os campos!');
            return;
        }

        if (this.isEdit) {
            this.categoryService.updateCategory(this.categoryForm.getRawValue()).subscribe(res => {
                this.messageService.success('Categoria editada com sucesso!');
                this.router.navigate(['dashboard/categories']);
            }, error => {
                this.messageService.error('Erro na edição da categoria, tente novamente ou entre em contato com o suporte.');
            });
            return ;
        }

        this.categoryService.saveCategory(this.categoryForm.getRawValue()).subscribe(res => {
            this.messageService.success('Categoria criada com sucesso!');
            this.router.navigate(['dashboard/categories']);
        }, error => {
            this.messageService.error('Erro na criação da categoria, tente novamente ou entre em contato com o suporte.');
        });
    }

}
