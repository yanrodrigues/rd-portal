import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { CategoryService } from 'src/app/shared/service/category.service';
import { Category } from 'src/app/models/category.model';
import { MatDialog } from '@angular/material';
import { ConfirmModalComponent } from 'src/app/shared/confirm-modal/confirm-modal.component';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

    categories$: Observable<any>;

    constructor(
        private categoryService: CategoryService,
        private dialog: MatDialog,
        private messageService: ToastrService
    ) { }

    ngOnInit() {
        this.getCategories();
    }

    private getCategories() {
        this.categories$ = this.categoryService.getAllCategory();
    }

    getImageUrl(category: Category): string {
        return category.image ? category.image.url : 'https://image.flaticon.com/icons/svg/21/21104.svg';
    }

    deleteCategory(id: number) {
        const data = {title: 'Excluir categoria', msg: 'Essa ação não pode ser desfeita, tem certeza?'};

        this.dialog.open(ConfirmModalComponent, { data }).afterClosed().subscribe(res => {
            if (res) {
                this.categoryService.deleteCategory(id).subscribe(response => {
                    this.messageService.success('Categoria excluida com sucesso');
                    this.getCategories();
                }, error => {
                    this.messageService.error('Erro ao excluir categoria, tente novamente ou contate o suporte.');
                });
            }
        });
    }

}
