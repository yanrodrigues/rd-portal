import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardLayoutComponent } from './dashboard-layout/dashboard-layout.component';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { SharedModule } from '../shared/shared.module';
import { MenuComponent } from './dashboard-layout/menu/menu.component';
import { ContentComponent } from './dashboard-layout/content/content.component';
import { UserComponent } from './dashboard-layout/menu/user/user.component';
import { HttpInterceptorsModule } from '../core/http-interceptor/http-interceptors.module';
import { HttpClientModule } from '@angular/common/http';
import { PermissionsGuard } from '../core/auth/permissions.guard';
import { NgxUiLoaderModule } from 'ngx-ui-loader';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [
      DashboardLayoutComponent,
      MenuComponent,
      ContentComponent,
      UserComponent
    ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    SharedModule,
    HttpClientModule,
    HttpInterceptorsModule,
    NgxUiLoaderModule,
    RouterModule
  ],
  providers: [
      PermissionsGuard
  ]
})
export class DashboardModule { }
