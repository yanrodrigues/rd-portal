import { Component, OnInit, Inject, PLATFORM_ID } from '@angular/core';
import { trigger, style, animate, transition } from '@angular/animations';
import { AuthService } from 'src/app/core/auth/auth.service';
import { SessionUser } from 'src/app/models/session-user.model';
import { NgxPermissionsService } from 'ngx-permissions';
import { isPlatformBrowser } from '@angular/common';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard-layout',
  templateUrl: './dashboard-layout.component.html',
  styleUrls: ['./dashboard-layout.component.scss'],
  animations: [
    trigger(
      'enterAnimation', [
        transition(':enter', [
          style({transform: 'translateX(-100%)', opacity: 0}),
          animate('100ms', style({transform: 'translateX(0)', opacity: 1}))
        ]),
        transition(':leave', [
          style({transform: 'translateX(0)', opacity: 0}),
          animate('100ms', style({transform: 'translateX(-100%)', opacity: 1}))
        ])
      ]
    )
  ],
})
export class DashboardLayoutComponent implements OnInit {

    showMenu = false;
    platform;
    isLoaded = false;

    constructor(
        private auth: AuthService,
        private permissionsService: NgxPermissionsService,
        @Inject(PLATFORM_ID) platformId: string,
        private router: Router
    ) {
        this.platform = platformId;
    }

    ngOnInit() {
        if (isPlatformBrowser(this.platform)) {
            this.auth.getUser().subscribe((user: SessionUser) => {
                if (user !== null) {
                    this.permissionsService.loadPermissions(user.authorities);
                } else {
                    this.router.navigate(['login']);
                }
            });
            this.isLoaded = true;
        }
    }

    toggleMenu() {
        this.showMenu = !this.showMenu;
    }

    hideMenu() {
        this.showMenu = false;
    }

}
