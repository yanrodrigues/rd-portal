import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss']
})
export class ContentComponent implements OnInit {

    @Output() toggleMenu = new EventEmitter();

    constructor() { }

    ngOnInit() {
    }

    toggleEvent() {
        this.toggleMenu.emit(true);
    }

}
