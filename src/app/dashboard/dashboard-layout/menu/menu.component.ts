import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthService } from 'src/app/core/auth/auth.service';
import { SessionUser } from 'src/app/models/session-user.model';
import { ActivatedRouteSnapshot, ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

    @Input() showMenu: boolean;
    @Output() toggleMenu = new EventEmitter();
    @Output() hideMenu = new EventEmitter();

    constructor(
        private auth: AuthService,
        private route: Router
        ) { }

    ngOnInit() {
    }

    hasRoute(url: string) {
        return this.route.url.includes(url);
    }

    isRoute(url: string) {
        return this.route.url === url;
    }

    toggleMenuEvent() {
        this.toggleMenu.emit(true);
    }

    hideMenuEvent() {
        this.hideMenu.emit(true);
    }

    logout() {
        this.auth.logout();
    }

}
