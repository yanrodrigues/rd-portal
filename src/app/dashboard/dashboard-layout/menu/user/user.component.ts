import { Component, OnInit, Inject, PLATFORM_ID } from '@angular/core';
import { AuthService } from 'src/app/core/auth/auth.service';
import { SessionUser } from 'src/app/models/session-user.model';
import { Role } from 'src/app/models/role.enum';
import { isPlatformBrowser } from '@angular/common';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

    user: SessionUser;
    role: string;
    platform;

    constructor(
        private auth: AuthService,
        @Inject(PLATFORM_ID) platformId: string
    ) {
        this.platform = platformId;
    }

    ngOnInit() {
        if (isPlatformBrowser(this.platform)) {
            this.auth.getUser().subscribe(user => {
                this.user = user;
                this.role = user.authorities.includes(Role.ROLE_ADMIN) ? 'Administrador' :
                            user.authorities.includes(Role.ROLE_MODERATOR) ? 'Moderador' :
                            user.authorities.includes(Role.ROLE_USER) ? 'Usuário' : 'Desconhecido';
            });
        }
    }

    getImageUrl(user: SessionUser): string {
        return user.image ? user.image.url : 'https://image.flaticon.com/icons/svg/21/21104.svg';
    }

}
