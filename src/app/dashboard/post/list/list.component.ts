import { Component, OnInit } from '@angular/core';
import { Category } from 'src/app/models/category.model';
import { AuthService } from 'src/app/core/auth/auth.service';
import { CategoryService } from 'src/app/shared/service/category.service';
import { SessionUser } from 'src/app/models/session-user.model';
import { Role } from 'src/app/models/role.enum';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

    categories: Category[];

    constructor(
        private authService: AuthService,
        private categoryService: CategoryService
    ) { }

    ngOnInit() {
        this.getCategories();
    }

    private getCategories() {
        this.authService.getUser().subscribe((user: SessionUser) => {
            if (user.authorities.includes(Role.ROLE_ADMIN) ||
                user.authorities.includes(Role.ROLE_MODERATOR)) {
                    this.categoryService.getAllCategory().subscribe(response => {
                        this.categories = response;
                    });
            } else {
                this.categories = user.categories;
            }
        });
    }

}
