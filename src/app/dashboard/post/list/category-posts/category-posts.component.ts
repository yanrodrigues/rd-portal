import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { Post } from 'src/app/models/post.model';
import { ConfirmModalComponent } from 'src/app/shared/confirm-modal/confirm-modal.component';
import { PostService } from 'src/app/shared/service/post.service';
import { MatDialog } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import { Category } from 'src/app/models/category.model';
import { Pageable } from 'src/app/models/pageable.model';

@Component({
  selector: 'app-category-posts',
  templateUrl: './category-posts.component.html',
  styleUrls: ['./category-posts.component.scss']
})
export class CategoryPostsComponent implements OnInit {

    @Input() category: Category;
    posts: Post[] = [];
    firstList: boolean;
    paging: Pageable = new Pageable(0, 4, 'id');

    constructor(
        private postService: PostService,
        private dialog: MatDialog,
        private messageService: ToastrService
    ) { }

    ngOnInit() {
        this.firstList = false;
        this.getPosts();
    }

    getPosts(addPage?: boolean) {
        if (addPage === true) {
            this.paging.pageNo = this.paging.pageNo + 1;
        }

        this.postService.getPostsByCategoryWithPagination(
            this.paging,
            this.category.id
        ).subscribe((posts: Post[]) => {
            this.firstList = true;
            if (!!posts.length) {
                posts.forEach(post => {
                    this.posts.push(post);
                });
            } else {
                if (this.firstList) {
                    this.messageService.info('Todos as postagens já estão listadas!');
                }
            }
        });
    }

    getImageUrl(post: Post): string {
        if (!!post.postText) {
            return post.postText.image ? post.postText.image.url : 'https://image.flaticon.com/icons/svg/21/21104.svg';
        }

        if (!!post.postForm) {
            return post.postForm.image ? post.postForm.image.url : 'https://image.flaticon.com/icons/svg/21/21104.svg';
        }
    }

    deletePost(id: number) {
        const data = {title: 'Excluir postagem', msg: 'Essa ação não pode ser desfeita, tem certeza?'};

        this.dialog.open(ConfirmModalComponent, { data }).afterClosed().subscribe(res => {
            if (res) {
                this.postService.deletePost(id).subscribe(response => {
                    this.messageService.success('Postagem excluida com sucesso');
                    this.getPosts();
                }, error => {
                    this.messageService.error('Erro ao excluir post, tente novamente ou contate o suporte.');
                });
            }
        });
    }

}
