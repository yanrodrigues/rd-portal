import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { PostRoutingModule } from './post-routing.module';
import { ListComponent } from './list/list.component';
import { RegisterComponent } from './register/register.component';
import { PostTextComponent } from './register/post-text/post-text.component';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { PostFormComponent } from './register/post-form/post-form.component';
import { CategoryPostsComponent } from './list/category-posts/category-posts.component';



@NgModule({
  declarations: [ListComponent, RegisterComponent, PostTextComponent, PostFormComponent, CategoryPostsComponent],
  imports: [
    CommonModule,
    SharedModule,
    PostRoutingModule,
    CKEditorModule
  ]
})
export class PostModule { }
