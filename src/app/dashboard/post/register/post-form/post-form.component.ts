import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { Post } from 'src/app/models/post.model';

@Component({
  selector: 'app-post-form',
  templateUrl: './post-form.component.html',
  styleUrls: ['./post-form.component.scss']
})
export class PostFormComponent implements OnInit, OnDestroy {

    @Input() parentForm: FormGroup;
    @Input() post: Post;
    formBase: FormGroup;
    questions: FormArray;

    constructor(
        private fb: FormBuilder
    ) { }

    ngOnInit() {
        if (!!this.post) {
            this.loadForm();
        } else {
            this.createForm();
        }
    }

    private createForm() {
        this.parentForm.addControl('postForm', this.fb.group({
            title: this.fb.control(null, [Validators.required]),
            subtitle: this.fb.control(null, [Validators.required]),
            image: this.fb.control(null, [Validators.required]),
            questions: this.fb.array([], [Validators.required])
        }));

        this.formBase = this.parentForm.get('postForm') as FormGroup;
        this.questions = this.formBase.get('questions') as FormArray;
    }

    private loadForm() {
        const post = this.post;
        this.parentForm.addControl('postForm', this.fb.group({
            title: this.fb.control(post.postForm.title, [Validators.required]),
            subtitle: this.fb.control(post.postForm.subtitle, [Validators.required]),
            image: this.fb.control(post.postForm.image, [Validators.required]),
            questions: this.fb.array([], [Validators.required])
        }));

        this.formBase = this.parentForm.get('postForm') as FormGroup;
        this.questions = this.formBase.get('questions') as FormArray;

        post.postForm.questions.forEach(question => {
            this.questions.push(this.fb.control(question, [Validators.required]));
        });
    }

    addQuestion() {
        this.questions.push(this.fb.control(null, [Validators.required]));
    }

    deleteQuestion(index: number) {
        this.questions.removeAt(index);
    }

    ngOnDestroy() {
        this.parentForm.removeControl('postText');
    }

}
