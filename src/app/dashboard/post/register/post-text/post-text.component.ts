import { Component, OnInit, Input, OnDestroy, Inject, PLATFORM_ID } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { CloudinaryImageUploadAdapter } from 'ckeditor-cloudinary-uploader-adapter';
import { ChangeEvent } from '@ckeditor/ckeditor5-angular';
import { Post } from 'src/app/models/post.model';
import { isPlatformBrowser } from '@angular/common';

@Component({
  selector: 'app-post-text',
  templateUrl: './post-text.component.html',
  styleUrls: ['./post-text.component.scss']
})
export class PostTextComponent implements OnInit, OnDestroy {

    @Input() parentForm: FormGroup;
    @Input() post: Post;
    formBase: FormGroup;
    ckEditorLoaded = false;
    public editor;
    editorConfig = {
      placeholder: 'Escreva sua postagem aqui!*',
      extraPlugins: [ this.imagePluginFactory ]
    };
    platform;

    constructor(
        private fb: FormBuilder,
        @Inject(PLATFORM_ID) platformId: string
    ) {
        this.platform = platformId;
     }

    ngOnInit() {
        if (!!this.post) {
            this.loadForm();
        } else {
            this.createForm();
        }
        if (isPlatformBrowser(this.platform)) {
          this.loadCkEditor();
        }
    }
    loadCkEditor() {
      const jsElmCK = document.createElement('script');
      jsElmCK.type = 'application/javascript';
      jsElmCK.src = 'https://cdn.ckeditor.com/ckeditor5/18.0.0/classic/ckeditor.js';
      document.body.appendChild(jsElmCK);
      jsElmCK.onload = () => {
        this.editor = (window as any).ClassicEditor;
        this.ckEditorLoaded = true;
      };
    }

    private createForm() {
        this.parentForm.addControl('postText', this.fb.group({
            title: this.fb.control(null, [Validators.required]),
            subtitle: this.fb.control(null, [Validators.required]),
            content: this.fb.control(null, [Validators.required]),
            image: this.fb.control(null, [Validators.required])
        }));

        this.formBase = this.parentForm.get('postText') as FormGroup;
    }

    private loadForm() {
        const post = this.post;
        this.parentForm.addControl('postText', this.fb.group({
            id: this.fb.control(post.postText.id, [Validators.required]),
            title: this.fb.control(post.postText.title, [Validators.required]),
            subtitle: this.fb.control(post.postText.subtitle, [Validators.required]),
            content: this.fb.control(post.postText.content, [Validators.required]),
            image: this.fb.control(post.postText.image, [Validators.required])
        }));

        this.formBase = this.parentForm.get('postText') as FormGroup;
    }

    contentChange({ editor }: ChangeEvent) {
        this.formBase.get('content').setValue(editor.getData());
    }

    imagePluginFactory(editor) {
        editor.plugins.get( 'FileRepository' ).createUploadAdapter = ( loader ) => {
        return new CloudinaryImageUploadAdapter( loader, 'rdportal', 'rdportaldefault');
        };
    }

    ngOnDestroy() {
        this.parentForm.removeControl('postText');
    }

}
