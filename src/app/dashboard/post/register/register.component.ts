import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CategoryService } from 'src/app/shared/service/category.service';
import { ToastrService } from 'ngx-toastr';
import { PostService } from 'src/app/shared/service/post.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Post } from 'src/app/models/post.model';
import { Category } from 'src/app/models/category.model';
import { AuthService } from 'src/app/core/auth/auth.service';
import { SessionUser } from 'src/app/models/session-user.model';
import { Role } from 'src/app/models/role.enum';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

    postType: FormGroup;
    isEdit: boolean;
    post: Post;

    categories: Category[];

    constructor(
        private fb: FormBuilder,
        private categoryService: CategoryService,
        private messageService: ToastrService,
        private postService: PostService,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private authService: AuthService
    ) { }

    ngOnInit() {
        const postId: number = parseInt(this.activatedRoute.snapshot.paramMap.get('id'));
        if (postId) {
            this.postService.getPost(postId).subscribe(res => {
                if (res) {
                    this.isEdit = true;
                    this.post = res;
                    this.loadPostTypeForm();
                    this.getCategories();
                } else {
                    this.router.navigate(['post']);
                }
            }, error => {
                this.router.navigate(['post']);
            });
        } else {
            this.isEdit = false;
            this.createPostTypeForm();
            this.getCategories();
        }
    }

    private createPostTypeForm() {
        this.postType = this.fb.group({
            type: this.fb.control(null, [Validators.required]),
            categories: this.fb.control(null, [Validators.required])
        });
    }

    private loadPostTypeForm() {
        const post = this.post;
        const type = this.post.postText ? 'postText' :
                        this.post.postForm ? 'postForm' : null;
        this.postType = this.fb.group({
            id: this.fb.control(post.id, [Validators.required]),
            type: this.fb.control(type, [Validators.required]),
            categories: this.fb.control(post.categories, [Validators.required])
        });
    }

    private getCategories() {
        this.authService.getUser().subscribe((user: SessionUser) => {
            if (user.authorities.includes(Role.ROLE_ADMIN) ||
                user.authorities.includes(Role.ROLE_MODERATOR)) {
                this.categoryService.getAllCategory().subscribe(response => {
                    this.categories = response;
                });
            } else {
                this.categories = user.categories;
            }
        });
    }

    onSubmit() {
        this.postType.markAllAsTouched();
        this.postType.updateValueAndValidity();

        if (this.postType.invalid) {
            const type = this.postType.get('type').value;

            if (type === 'postText') {
                if (this.postType.get('postText').get('content').invalid) {
                    this.messageService.error('Preencha o conteúdo da postagem!');
                    return;
                }
            }

            if (this.postType.get(type).get('image').invalid) {
                this.messageService.error('Envie uma imagem para a postagem!');
                return;
            }

            this.messageService.error('Preencha todos os campos obrigatórios!');
            return;
        }

        if (this.isEdit) {
            this.postService.updatePost(this.postType.getRawValue()).subscribe(res => {
                this.messageService.success('Postagem editada com sucesso!');
                this.router.navigate(['dashboard/post']);
            }, error => {
                this.messageService.error('Erro na edição da postagem, tente novamente ou entre em contato com o suporte.');
            });
            return ;
        }

        this.postService.savePost(this.postType.getRawValue()).subscribe(response => {
            this.messageService.success('Post criado com sucesso!');
            this.router.navigate(['dashboard/post']);
        }, error => {
            this.messageService.error('Erro na criação do post, tente novamente ou entre em contato com o suporte.');
        });
    }

    compareObjects(o1: any, o2: any): boolean {
        return o1.name === o2.name && o1.id === o2.id;
    }

}
