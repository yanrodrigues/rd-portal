import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { SessionUser } from 'src/app/models/session-user.model';
import { Observable } from 'rxjs';
import { Authority } from 'src/app/models/authority.model';
import { Category } from 'src/app/models/category.model';
import { AuthorityService } from 'src/app/shared/service/authority.service';
import { ToastrService } from 'ngx-toastr';
import { UserService } from 'src/app/shared/service/user.service';
import { CategoryService } from 'src/app/shared/service/category.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/core/auth/auth.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

    userForm: FormGroup;
    user: SessionUser;
    authorities: Authority[];
    categories: Category[];
    isEdit: boolean;

    constructor(
        private fb: FormBuilder,
        private authorityService: AuthorityService,
        private messageService: ToastrService,
        private userService: UserService,
        private categoryService: CategoryService,
        private router: Router,
        private auth: AuthService
    ) { }

    ngOnInit() {
        this.auth.getUser().subscribe(user => {
            this.isEdit = true;
            this.user = user;
            this.authorityService.getAuthorities().subscribe(authorities => {
                this.authorities = authorities;
                this.categoryService.getAllCategory().subscribe(categories => {
                    this.categories = categories;
                    this.loadForm();
                });
            });
        });
    }

    private loadForm() {
        const user = this.user;
        const authorities = [];
        const categories = [];

        this.authorities.forEach(authority => {
            let status = false;
            user.authorities.forEach(userAuthority => {
                if (authority.name === userAuthority && !status) {
                    authorities.push(authority);
                    status = true;
                }
            });
        });

        user.authorities = authorities;

        this.userForm = this.fb.group({
            id: this.fb.control(user.id, [Validators.required]),
            username: this.fb.control(user.username, [Validators.required, Validators.min(4), Validators.max(20)]),
            password: this.fb.control(user.password, [Validators.min(4), Validators.max(20)]),
            nomeCompleto: this.fb.control(user.nomeCompleto, [Validators.required, Validators.min(10)]),
            email: this.fb.control(user.email, [Validators.required, Validators.min(10)]),
            authorities: this.fb.control(user.authorities, [Validators.required]),
            categories: this.fb.control(user.categories),
            image: this.fb.control(user.image, [Validators.required]),
            publicName: this.fb.control(user.publicName, [Validators.required]),
            publicDescription: this.fb.control(user.publicDescription, [Validators.required])
        });
    }

    compareObjects(o1: any, o2: any): boolean {
        return o1.name === o2.name && o1.id === o2.id;
    }

    onSubmit() {
        if (this.userForm.invalid) {
            this.messageService.error('Preencha todos os campos!');
            return;
        }

        this.userService.updateUser(this.userForm.getRawValue()).subscribe(res => {
            this.auth.updateUser().subscribe(user => {
                this.messageService.success('Perfil editado com sucesso!');
                this.router.navigate(['dashboard']);
            });
        }, error => {
            this.messageService.error('Erro na edição do perfil, tente novamente ou entre em contato com o suporte.');
        });
        return ;
    }

}
