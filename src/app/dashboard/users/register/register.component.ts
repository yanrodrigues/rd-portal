import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { Authority } from 'src/app/models/authority.model';
import { AuthorityService } from 'src/app/shared/service/authority.service';
import { ToastrService } from 'ngx-toastr';
import { UserService } from 'src/app/shared/service/user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { SessionUser } from 'src/app/models/session-user.model';
import { Route } from '@angular/compiler/src/core';
import { Category } from 'src/app/models/category.model';
import { CategoryService } from 'src/app/shared/service/category.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

    userForm: FormGroup;
    user: SessionUser;
    authorities$: Observable<Authority[]>;
    categories$: Observable<Category[]>;

    isEdit: boolean;

    constructor(
        private fb: FormBuilder,
        private authorityService: AuthorityService,
        private messageService: ToastrService,
        private userService: UserService,
        private categoryService: CategoryService,
        private router: Router,
        private activatedRoute: ActivatedRoute
    ) { }

    ngOnInit() {
        const userId: number = parseInt(this.activatedRoute.snapshot.paramMap.get('id'));
        if (userId) {
            this.userService.getUser(userId).subscribe(res => {
                if (res) {
                    this.isEdit = true;
                    this.user = res;
                    this.loadForm();
                    this.getAuthorities();
                    this.getCategories();
                } else {
                    this.router.navigate(['users']);
                }
            }, error => {
                this.router.navigate(['users']);
            });
        } else {
            this.isEdit = false;
            this.createForm();
            this.getAuthorities();
            this.getCategories();
        }
    }

    private createForm() {
        this.userForm = this.fb.group({
            username: this.fb.control(null, [Validators.required, Validators.min(4), Validators.max(20)]),
            password: this.fb.control(null, [Validators.required, Validators.min(4), Validators.max(20)]),
            nomeCompleto: this.fb.control(null, [Validators.required, Validators.min(10)]),
            email: this.fb.control(null, [Validators.required, Validators.min(10)]),
            authorities: this.fb.control(null, [Validators.required]),
            categories: this.fb.control(null),
            image: this.fb.control(null, [Validators.required]),
            publicName: this.fb.control(null, [Validators.required]),
            publicDescription: this.fb.control(null, [Validators.required])
        });
    }

    private loadForm() {
        const user = this.user;
        this.userForm = this.fb.group({
            id: this.fb.control(user.id, [Validators.required]),
            username: this.fb.control(user.username, [Validators.required, Validators.min(4), Validators.max(20)]),
            password: this.fb.control(user.password, [Validators.min(4), Validators.max(20)]),
            nomeCompleto: this.fb.control(user.nomeCompleto, [Validators.required, Validators.min(10)]),
            email: this.fb.control(user.email, [Validators.required, Validators.min(10)]),
            authorities: this.fb.control(user.authorities, [Validators.required]),
            categories: this.fb.control(user.categories),
            image: this.fb.control(user.image, [Validators.required]),
            publicName: this.fb.control(user.publicName, [Validators.required]),
            publicDescription: this.fb.control(user.publicDescription, [Validators.required])
        });
    }

    private getAuthorities() {
        this.authorities$ = this.authorityService.getAuthorities();
    }

    private getCategories() {
        this.categories$ = this.categoryService.getAllCategory();
    }

    compareObjects(o1: any, o2: any): boolean {
        return o1.name === o2.name && o1.id === o2.id;
    }

    onSubmit() {
        if (this.userForm.invalid) {
            this.messageService.error('Preencha todos os campos!');
            return;
        }

        if (this.isEdit) {
            this.userService.updateUser(this.userForm.getRawValue()).subscribe(res => {
                this.messageService.success('Usuário editado com sucesso!');
                this.router.navigate(['dashboard/users']);
            }, error => {
                this.messageService.error('Erro na edição do usuário, tente novamente ou entre em contato com o suporte.');
            });
            return ;
        }

        this.userService.saveUser(this.userForm.getRawValue()).subscribe(res => {
            this.messageService.success('Usuário criado com sucesso!');
            this.router.navigate(['dashboard/users']);
        }, error => {
            this.messageService.error('Erro na criação do usuário, tente novamente ou entre em contato com o suporte.');
        });
    }

}
