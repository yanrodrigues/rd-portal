import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { UserService } from 'src/app/shared/service/user.service';
import { ToastrService } from 'ngx-toastr';
import { MatDialog } from '@angular/material';
import { ConfirmModalComponent } from 'src/app/shared/confirm-modal/confirm-modal.component';
import { SessionUser } from 'src/app/models/session-user.model';
import { AuthService } from 'src/app/core/auth/auth.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

    users$: Observable<any>;
    userLogged: SessionUser;

    constructor(
        private userService: UserService,
        private messageService: ToastrService,
        private dialog: MatDialog,
        private authService: AuthService
    ) { }

    ngOnInit() {
        this.authService.getUser().subscribe((user: SessionUser) => {
            this.userLogged = user;
            this.getUsers();
        });
    }

    private getUsers() {
        this.users$ = this.userService.getAllUsers();
    }

    getImageUrl(user: SessionUser): string {
        return user.image ? user.image.url : 'https://image.flaticon.com/icons/svg/21/21104.svg';
    }

    deleteUser(id: number) {
        const data = {title: 'Excluir usuário', msg: 'Essa ação não pode ser desfeita, tem certeza?'};

        this.dialog.open(ConfirmModalComponent, { data }).afterClosed().subscribe(res => {
            if (res) {
                this.userService.deleteUser(id).subscribe(response => {
                    this.messageService.success('Usuário excluido com sucesso');
                    this.getUsers();
                }, error => {
                    this.messageService.error('Erro ao excluir usuário, tente novamente ou contate o suporte.');
                });
            }
        });
    }

}
